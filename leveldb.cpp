#include<leveldb/db.h>



int main(){
    leveldb::DB* db;
    leveldb::Options options;
    leveldb::Status status = leveldb::DB::Open(options, "/tmp/testdb", &db);
    assert(status.ok());

    std::string value;
    std::string key = "x1";
    leveldb::Status s = db->Get(leveldb::ReadOptions(), key, &value);
    if(s.ok()){
        s = db->Put(leveldb::WriteOptions(), key, value);
    }else{
        s = db->Put(leveldb::WriteOptions(), key);
    }

    // atomic updates
    leveldb::WriteBatch batch;
    batch.Delete(key1);
    batch.Put(key2,value);
    s = db->Write(leveldb::WriteOptions(), &batch);

    // synchronous

    // concurrency

    leveldb::Iterator* it = db->NewIterator(leveldb::ReadOptions());
    // it->SeekToLast()
    for(it->SeekToFirst(); it->Valid(); it->Next()){
        std::cout << it->key().ToString() << ":" << it->value().ToString() << std::endl;
    }


    // snapshots
    leveldb::ReadOptions options;
    options.snapshot = db->GetSnapshot();
    leveldb::Iterator * iter = db->NewIterator(options);
    delete iter;
    db->ReleaseSnapshot(options.snapshot);

    // leveldb::Slice type,
    leveldb::Slice s1 = "hello";

    std::string str("world");
    leveldb::Slice s2 = str;

    // a slice can be easily converted back to a C++ string:





}
