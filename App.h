#pragma once
#include "Logger.h"
#include <string>

enum{
  NGX_OK,
  NGX_PROCESS_JUST_RESPAWN,
  NGX_PROCESS_RESPAWN,
  NGX_LOG_DEBUG_EVENT,
  NGX_ESRCH,
  NGX_FILE_ERROR,
};
struct Processe{
  bool detached;
  int pid;
  bool exiting;
  bool just_spwan;
  bool exited;
  int channel[2];
  bool respawn;
  int proc;
  void* data;
  std::string name;
};

struct APP{
  int err;
  int ngx_new_binary;
  int delay;
  int sigio;
  int live;
  int ngx_sigalrm;
  int ngx_change_binary;
  int  ngx_noaccepting;
  int  ngx_reconfigure;
  int ngx_restart;
  int ngx_reopen;
  int ngx_reap;
  int ngx_noaccept;
  bool ngx_terminate;
  bool ngx_quit;
  char ** ngx_argv;
  int ngx_argc;

  int ngx_last_process;

  Processe *ngx_processes;

  bool detached;



};
int ngx_write_channel(int fd, void* ch,int size , Logger&);
int ngx_close_channel(int fd, void *ch, int size, Logger&);

extern APP App;
extern struct itimerval itv;
