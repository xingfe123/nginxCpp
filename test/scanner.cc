//
//In addition to standard headers, the Flex scanner includes the driver’s, then the parser’s to get the set of defined tokens.

%{ /* -*- C++ -*- */
# include <cerrno>
# include <climits>
# include <cstdlib>
# include <cstring> // strerror
# include <string>
# include "driver.hh"
# include "parser.hh"
%}

//Since our calculator has no #include-like feature, we don’t need yywrap. We
//don’t need the unput and input functions either, and we parse an actual file,
//this is not an interactive session with the user. Finally, we enable scanner
//tracing.

%option noyywrap nounput noinput batch debug

 //The following function will be handy to convert a string denoting a number into a number token.

%{
  // A number symbol corresponding to the value in S.
  yy::parser::symbol_type
  make_NUMBER (const std::string &s, const yy::parser::location_type& loc);
%}

//Abbreviations allow for more readable rules.

id    [a-zA-Z][a-zA-Z_0-9]*
int   [0-9]+
blank [ \t]
// The following paragraph suffices to track locations accurately. Each time
// yylex is invoked, the begin position is moved onto the end position. Then
// when a pattern is matched, its width is added to the end column. When
// matching ends of lines, the end cursor is adjusted, and each time blanks are
// matched, the begin cursor is moved onto the end cursor to effectively ignore
// the blanks preceding tokens. Comments would be treated equally.

%{
  // Code run each time a pattern is matched.
  # define YY_USER_ACTION  loc.columns (yyleng);
%}

%%
%{
  // A handy shortcut to the location held by the driver.
  yy::location& loc = drv.location;
  // Code run each time yylex is called.
  loc.step ();
%}

{blank}+   loc.step ();
\n+        loc.lines (yyleng); loc.step ();
//The rules are simple. The driver is used to report errors.

"-"        return yy::parser::make_MINUS(loc);
"+"        return yy::parser::make_PLUS(loc);
"*"        return yy::parser::make_STAR(loc);
"/"        return yy::parser::make_SLASH(loc);
"("        return yy::parser::make_LPAREN(loc);
")"        return yy::parser::make_RPAREN(loc);
":="       return yy::parser::make_ASSIGN(loc);

{int}      return make_NUMBER(yytext, loc);
{id}       return yy::parser::make_IDENTIFIER (yytext, loc);
.          {
             throw yy::parser::syntax_error
               (loc, "invalid character: " + std::string(yytext));
}

<<EOF>>    return yy::parser::make_END (loc);
%%
 //You should keep your rules simple, both in the parser and in the scanner. Throwing from the auxiliary functions is then very handy to report errors.

yy::parser::symbol_type make_NUMBER (const std::string &s, const yy::parser::location_type& loc){
    errno = 0;
    long n = strtol(s.c_str(), NULL, 10);
    if (! (std::numeric_limits<long>:min() <= n
           && n <= std::numeric_limits<long>:max()
           && errno != ERANGE)){
        throw yy::parser::syntax_error (loc, "integer is out of range: " + s);
    }

    return yy::parser::make_NUMBER((int) n, loc);
}
//Finally, because the scanner-related driver’s member-functions depend on the scanner’s data, it is simpler to implement them in this file.

void driver::scan_begin(){
    yy_flex_debug = trace_scanning;
    if (file.empty () || file == "-")
        yyin = stdin;
    else if (!(yyin = fopen (file.c_str (), "r")))
    {
        std::cerr << "cannot open " << file << ": " << strerror(errno) << '\n';
        exit (EXIT_FAILURE);
    }
}

void driver::scan_end(){
    fclose(yyin);
}
