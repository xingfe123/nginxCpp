#include<leveldb/db.h>
#include <iostream>


struct LevelMap{
    leveldb::DB* db;
    leveldb::Options options;
    leveldb::Status status;
    LevelMap(){
        status = leveldb::DB::Open(options, "/tmp/testdb", &db);
    }
    
    void put(const std::string& key, const std::string& value){
        if(status.ok()){
            throw "";
        }

        status =  db->Put(leveldb::WriteOptions(), key, value);
        
        if(status.ok()){
            throw "";
        }
    }

    std::string get(const std::string & key){
        return "";
    }

    void putBatch(leveldb::WriteBatch* batch){
        //leveldb::WriteBatch batch;
        //  batch.Delete(key1);
        ///  batch.Put(key2,value);
        status = db->Write(leveldb::WriteOptions(), batch);
        
        if(status.ok()){
            throw "";
        }
    }

    struct Iterator{
        leveldb::Iterator* it;
        Iterator(leveldb::Iterator* it_):
                it(it_){
            it->SeekToFirst();
        }
        Iterator operator++(){
            it->Next();
            return *this;
        }
        bool operator!=(const Iterator& right) const{
            return it->Valid();
        }
        bool operator==(const Iterator& right) const{
            return !it->Valid();
        }
        leveldb::Iterator& operator*(){
            return *(this->it);
        }
        leveldb::Iterator* operator->(){
            return this->it;
        }
    };
    Iterator begin(){
        return Iterator(db->NewIterator(leveldb::ReadOptions()));
    }
    Iterator end(){
        return Iterator(nullptr);
    }

};




int main(){

    //assert(status.ok());


    // atomic updates

    // synchronous

    // concurrency
    LevelMap leveldb;
    for(auto it = leveldb.begin();
        it != leveldb.end();
        ++it){
        std::cout << it->key().ToString() << ":" << it->value().ToString() << std::endl;
    }


    // snapshots
    // leveldb::ReadOptions options;
    // options.snapshot = db->GetSnapshot();
    // leveldb::Iterator * iter = db->NewIterator(options);
    // delete iter;
    // db->ReleaseSnapshot(options.snapshot);

    // leveldb::Slice type,
    // leveldb::Slice s1 = "hello";

    // std::string str("world");
    // leveldb::Slice s2 = str;

    // a slice can be easily converted back to a C++ string:

}
