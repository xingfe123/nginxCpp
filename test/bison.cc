// To support a pure interface with the scanner the technique of the “parsing
// context” is convenient: a structure containing all the data to
// exchange. Since, in addition to simply launch the parsing, there are several
// auxiliary tasks to execute (open the file for scanning, instantiate the
// parser etc.), we recommend transforming the simple parsing context structure
// into a fully blown parsing driver class.


#include "driver.hh"
#include "parser.hh"

driver::driver ()
  : trace_parsing (false), trace_scanning (false){
  variables["one"] = 1;
  variables["two"] = 2;
}
//The parse member function deserves some attention.

int driver::parse (const std::string &fileName){
    file = f;
    location.initialize (&file);
    scan_begin ();
    yy::parser parse (*this);
    parse.set_debug_level (trace_parsing);
    int res = parse ();
    scan_end ();
    return res;
}
