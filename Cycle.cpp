#include "Cycle.h"
#include "Logger.h"
#include "Util.h"
#include <signal.h>
#include "App.h"

static void run_query(const char* host,
                      const char*user,
                      const char * password){
    int err;

    int status = mysql_real_connect_start(&ret, &mysql,
                                          host, user,
                                          password, NULL, );

}

enum {
    NGX_RECONFIGURE_SIGNAL,
    NGX_REOPEN_SIGNAL,
    NGX_TERMINATE_SIGNAL,
    NGX_SHUTDOWN_SIGNAL,
    NGX_CHANGEBIN_SIGNAL,
    NGX_NOACCEPT_SIGNAL,
    NGX_LOG_ALERT,
    ngx_errno,
    NGX_CMD_OPEN_CHANNEL,
    ngx_process_slot,
    ngx_worker_process_cycle,
};

int ngx_signal_value(int s){
    return -1;
}
void CycleT::ngx_delete_pidfile(){
    //todo
}
typedef struct ngx_exit_log_file_s{
    int fd;
}ngx_exit_log_file ;

typedef struct ngx_exit_cycle_s{
    int  log;
    int files;

} ngx_exit_cycle;



void CycleT::close_listening_sockets() {
    //todo
}
void
CycleT::master_process_exit(){
    this->ngx_delete_pidfile();

    //ngx_log_error(NGX_LOG_NOTICE, this->log, 0, "exit");

    for (int i = 0; this->modules[i]; i++) {
        if (this->modules[i]->exit_master) {
            this->modules[i]->exit_master(this);
        }
    }

    this->close_listening_sockets();

    /*
     * Copy ngx_this->log related data to the special static exit cycle,
     * log, and log file structures enough to allow a signal handler to log.
     * The handler may be called when standard ngx_this->log allocated from
     * ngx_this->pool is already destroyed.
     */


    Logger ngx_exit_log = *this->log.get_file_log();

    //ngx_exit_log_file.fd = ngx_exit_log.file->fd;
    //ngx_exit_log.file = &ngx_exit_log_file;
    ngx_exit_log.next = NULL;
    ngx_exit_log.writer = NULL;

    printf("%s", ngx_exit_log.next!= NULL?"":"");
    // ngx_exit_cycle.log = &ngx_exit_log;
    // ngx_exit_cycle.files = ngx_this->files;
    // ngx_exit_cycle.files_n = ngx_this->files_n;
    // ngx_cycle = &ngx_exit_cycle;

    this->pool.destroy();

    exit(0);
}

int  CycleT::masterProcess(){
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGCHLD);
    sigaddset(&set, SIGALRM);
    sigaddset(&set, SIGIO);
    sigaddset(&set, SIGINT);
    sigaddset(&set, ngx_signal_value(NGX_RECONFIGURE_SIGNAL));
    sigaddset(&set, ngx_signal_value(NGX_REOPEN_SIGNAL));
    sigaddset(&set, ngx_signal_value(NGX_NOACCEPT_SIGNAL));
    sigaddset(&set, ngx_signal_value(NGX_TERMINATE_SIGNAL));
    sigaddset(&set, ngx_signal_value(NGX_SHUTDOWN_SIGNAL));
    sigaddset(&set, ngx_signal_value(NGX_CHANGEBIN_SIGNAL));

    if (sigprocmask(SIG_BLOCK, &set, NULL) == -1) {
        ngx_log_error(NGX_LOG_ALERT, this->log, ngx_errno,
                      "sigprocmask() failed");
    }

    sigemptyset(&set);


    int size = sizeof(master_process);

    for (int i = 0; i < App.ngx_argc; i++) {
        size += strlen(App.ngx_argv[i]) + 1;
    }

    char * title = this->pool.pnalloc( size);
    assert(title != NULL && "failed");


    char * p = (char*)memmove(title, master_process, sizeof(master_process) - 1);
    for (int i = 0; i < App.ngx_argc; i++) {
        *p++=' ';
        p = (char*)memmove(p, (u_char *) App.ngx_argv[i], size);
    }

    ngx_setproctitle(title);


    ngx_core_conf_t* ccf = this->configureContext.getConf(ngx_core_module);

    this->start_worker_processes(ccf->worker_processes,
                                 NGX_PROCESS_RESPAWN);
    this->start_cache_manager_processes(0);

    App.ngx_new_binary = 0;
    App.delay = 0;
    App.sigio = 0;
    App.live = 1;

    while(true){
        if (App.delay) {
            if (App.ngx_sigalrm) {
                App.sigio = 0;
                App.delay *= 2;
                App.ngx_sigalrm = 0;
            }

            // ngx_log_debug1(NGX_LOG_DEBUG_EVENT, this->log, 0,
            //                "termination cycle: %M", delay);

            itv.it_interval.tv_sec = 0;
            itv.it_interval.tv_usec = 0;
            itv.it_value.tv_sec = App.delay / 1000;
            itv.it_value.tv_usec = (App.delay % 1000 ) * 1000;

            if (setitimer(ITIMER_REAL, &itv, NULL) == -1) {
                ngx_log_error(NGX_LOG_ALERT, this->log, ngx_errno,
                              "setitimer() failed");
            }
        }

//        ngx_log_debug0(NGX_LOG_DEBUG_EVENT, this->log, 0, "sigsuspend");

        sigsuspend(&set);

        ngx_time_update();

        //      ngx_log_debug1(NGX_LOG_DEBUG_EVENT, this->log, 0,
        //"wake up, sigio %i", sigio);

        if (App.ngx_reap) {
            App.ngx_reap = 0;
            //ngx_log_debug0(NGX_LOG_DEBUG_EVENT, this->log, 0, "reap children");

            App.live = this->ngx_reap_children();
        }

        if (!App.live && (App.ngx_terminate || App.ngx_quit)) {
            this->master_process_exit();
        }

        if (App.ngx_terminate) {
            if (App.delay == 0) {
                App.delay = 50;
            }

            if (App.sigio) {
                App.sigio--;
                continue;
            }

            App.sigio = ccf->worker_processes + 2 /* cache processes */;

            if (App.delay > 1000) {
                this->signal_worker_processes( SIGKILL);
            } else {
                this->signal_worker_processes(
                                       ngx_signal_value(NGX_TERMINATE_SIGNAL));
            }

            continue;
        }

        if (App.ngx_quit) {
            this->signal_worker_processes(
                    ngx_signal_value(NGX_SHUTDOWN_SIGNAL));

            Fd *ls = this->listening.elts;
            for (int n = 0; n < this->listening.nelts; n++) {
                if (ngx_close_socket(ls[n].fd) == -1) {
                    //ngx_log_error(NGX_LOG_EMERG, this->log, ngx_socket_errno,
                    //ngx_close_socket_n " %V failed",
                    //            &ls[n].addr_text);
                }
            }
            this->listening.nelts = 0;

            continue;
        }

        if (App.ngx_reconfigure) {
            App.ngx_reconfigure = 0;

            if (App.ngx_new_binary) {
                this->start_worker_processes(ccf->worker_processes,
                                           NGX_PROCESS_RESPAWN);
                this->start_cache_manager_processes(0);
                App.ngx_noaccepting = 0;

                continue;
            }

            //ngx_log_error(NGX_LOG_NOTICE, this->log, 0, "reconfiguring");

            this->ngx_init_cycle();
            // if (cycle == NULL) {
            //     //cycle = (ngx_cycle_t *) ngx_cycle;
            //     continue;
            // }

            //ngx_cycle = cycle;
            ccf = (ngx_core_conf_t *) this->configureContext.getConf(ngx_core_module);

            this->start_worker_processes(ccf->worker_processes,
                                       NGX_PROCESS_JUST_RESPAWN);
            this->start_cache_manager_processes( 1);

            /* allow new processes to start */
            ngx_msleep(100);

            App.live = 1;
            this->signal_worker_processes(
                    ngx_signal_value(NGX_SHUTDOWN_SIGNAL));
        }

        if (App.ngx_restart) {
            App.ngx_restart = 0;
            this->start_worker_processes(ccf->worker_processes,
                                       NGX_PROCESS_RESPAWN);
            this->start_cache_manager_processes( 0);
            App.live = 1;
        }

        if (App.ngx_reopen) {
            App.ngx_reopen = 0;
            //ngx_log_error(NGX_LOG_NOTICE, this->log, 0, "reopening logs");
            this->reopen_files(ccf->user);
            this->signal_worker_processes(
                                        ngx_signal_value(NGX_REOPEN_SIGNAL));
        }

        if (App.ngx_change_binary) {
            App.ngx_change_binary = 0;
            //ngx_log_error(NGX_LOG_NOTICE, this->log, 0, "changing binary");
            App.ngx_new_binary = this->exec_new_binary(App.ngx_argv);
        }

        if (App.ngx_noaccept) {
            App.ngx_noaccept = 0;
            App.ngx_noaccepting = 1;
            this->signal_worker_processes(
                    ngx_signal_value(NGX_SHUTDOWN_SIGNAL));
        }
    }
}


void
CycleT::start_worker_processes(int n, int type){

    ngx_channel_t  ch;

    //ngx_log_error(NGX_LOG_NOTICE, cycle->log, 0, "start worker processes");

    bzero(&ch, sizeof(ngx_channel_t));

    ch.command = NGX_CMD_OPEN_CHANNEL;

    for (int i = 0; i < n; i++) {

        this->spawn_process(ngx_worker_process_cycle,
                          (void *) (intptr_t) i, "worker process", type);

        ch.pid = App.ngx_processes[ngx_process_slot].pid;
        ch.slot = ngx_process_slot;
        ch.fd = App.ngx_processes[ngx_process_slot].channel[0];

        this->pass_open_channel(ch);
    }

}

int CycleT::spawn_process(int cycle, void* , const std::string workerName, int type){
    //todo
    return -1;
}

void CycleT::pass_open_channel(ngx_channel_t &ch){
    //todo
}

void CycleT::start_cache_manager_processes(int) {
    //todo
}
enum {
    NGX_CMD_CLOSE_CHANNEL,
    NGX_LOG_DEBUG_CORE,
    NGX_INVALID_PID,
    ngx_new_binary,
};
int CycleT::ngx_reap_children(){
    ngx_channel_t     ch;
    ngx_core_conf_t  *ccf;

    bzero(&ch, sizeof(ngx_channel_t));

    ch.command = NGX_CMD_CLOSE_CHANNEL;
    ch.fd = -1;

    int live = 0;
    for (int i = 0; i < App.ngx_last_process; i++) {

        // ngx_log_debug7(NGX_LOG_DEBUG_EVENT, cycle->log, 0,
        //                "child: %i %P e:%d t:%d d:%d r:%d j:%d",
        //                i,
        //                ngx_processes[i].pid,
        //                ngx_processes[i].exiting,
        //                ngx_processes[i].exited,
        //                ngx_processes[i].detached,
        //                ngx_processes[i].respawn,
        //                ngx_processes[i].just_spawn);

        if (App.ngx_processes[i].pid == -1) {
            continue;
        }

        if (App.ngx_processes[i].exited) {

            if (!App.ngx_processes[i].detached) {
                //ngx_close_channel(App.ngx_processes[i].channel, this->log);

                App.ngx_processes[i].channel[0] = -1;
                App.ngx_processes[i].channel[1] = -1;

                ch.pid = App.ngx_processes[i].pid;
                ch.slot = i;

                for (int n = 0; n < App.ngx_last_process; n++) {
                    if (App.ngx_processes[n].exited
                        || App.ngx_processes[n].pid == -1
                        || App.ngx_processes[n].channel[0] == -1)
                    {
                        continue;
                    }

                    // ngx_log_debug3(NGX_LOG_DEBUG_CORE, this->log, 0,
                    //                "pass close channel s:%i pid:%P to:%P",
                    //                ch.slot, ch.pid, ngx_processes[n].pid);

                    /* TODO: NGX_AGAIN */

                    ngx_write_channel(App.ngx_processes[n].channel[0],
                                      &ch, sizeof(ngx_channel_t), this->log);
                }
            }

            if (App.ngx_processes[i].respawn
                && !App.ngx_processes[i].exiting
                && !App.ngx_terminate
                && !App.ngx_quit)
            {
                if (this->spawn_process(App.ngx_processes[i].proc,
                                        App.ngx_processes[i].data,
                                        App.ngx_processes[i].name, i)
                    == NGX_INVALID_PID)
                {
                    // ngx_log_error(NGX_LOG_ALERT, this->log, 0,
                    //               "could not respawn %s",
                    //               App.ngx_processes[i].name);
                    continue;
                }


                ch.command = NGX_CMD_OPEN_CHANNEL;
                ch.pid = App.ngx_processes[ngx_process_slot].pid;
                ch.slot = ngx_process_slot;
                ch.fd = App.ngx_processes[ngx_process_slot].channel[0];

                //this->ngx_pass_open_channel(&ch);

                live = 1;

                continue;
            }

            if (App.ngx_processes[i].pid == ngx_new_binary) {

                ccf = (ngx_core_conf_t *) this->configureContext.getConf(ngx_core_module);


                if (ngx_rename_file((char *) ccf->oldpid.data,
                                    (char *) ccf->pid.data)
                    == NGX_FILE_ERROR)
                {
                    // ngx_log_error(NGX_LOG_ALERT, cycle->log, ngx_errno,
                    //               ngx_rename_file_n " %s back to %s failed "
                    //               "after the new binary process \"%s\" exited",
                    //               ccf->oldpid.data, ccf->pid.data, ngx_argv[0]);
                }

                App.ngx_new_binary = 0;
                if (App.ngx_noaccepting) {
                    App.ngx_restart = 1;
                    App.ngx_noaccepting = 0;
                }
            }

            if (i == App.ngx_last_process - 1) {
                App.ngx_last_process--;

            } else {
                App.ngx_processes[i].pid = -1;
            }

        } else if (App.ngx_processes[i].exiting ||
                   !App.ngx_processes[i].detached) {
            live = 1;
        }
    }

    return live;
}

enum{
    NGX_CMD_QUIT,
    NGX_CMD_TERMINATE,
    NGX_CMD_REOPEN,
};

#define ngx_signal_helper(n)     SIG##n
#define ngx_signal_value(n)      ngx_signal_helper(n)
#define NGX_SHUTDOWN_SIGNAL      QUIT
#define NGX_TERMINATE_SIGNAL     TERM
#define NGX_NOACCEPT_SIGNAL      WINCH
#define NGX_RECONFIGURE_SIGNAL   HUP

#define NGX_REOPEN_SIGNAL        USR1
#define NGX_CHANGEBIN_SIGNAL     USR2



void CycleT::signal_worker_processes(int signo){
    ngx_channel_t ch;
    bzero(&ch, sizeof(ch));

    ch.command = 0;
    switch(signo){
    case ngx_signal_value(NGX_SHUTDOWN_SIGNAL):
        ch.command = NGX_CMD_QUIT;
        case ngx_signal_value(NGX_TERMINATE_SIGNAL):
        ch.command = NGX_CMD_TERMINATE;
        break;

    case ngx_signal_value(NGX_REOPEN_SIGNAL):
        ch.command = NGX_CMD_REOPEN;
        break;
    default:
        ch.command = 0;
        break;
    };


    ch.fd = -1;
    for (int i=0;i<App.ngx_last_process;i++){
        if(App.ngx_processes[i].detached || App.ngx_processes[i].pid == -1){
            continue;
        }

        if(App.ngx_processes[i].just_spwan){
            App.ngx_processes[i].just_spwan = 0;
            continue;
        }

        if(App.ngx_processes[i].exiting && signo == ngx_signal_value(NGX_SHUTDOWN_SIGNAL)){
            continue;
        }

        if(ch.command){
            if(ngx_write_channel(App.ngx_processes[i].channel[0],
                                 &ch, sizeof(ngx_channel_t), this->log) == NGX_OK){
                if(signo != ngx_signal_value(NGX_REOPEN_SIGNAL)){
                    App.ngx_processes[i].exiting = 1;
                }
                continue;
            }

        }

        int ret = kill(App.ngx_processes[i].pid, signo);
        if(ret == -1){
            if(App.err == NGX_ESRCH){
                App.ngx_processes[i].exited = 1;
                App.ngx_processes[i].exiting = 0;
                App.ngx_reap = 1;
            }

            continue;
        }

        if(signo != ngx_signal_value(NGX_REOPEN_SIGNAL)){
            App.ngx_processes[i].exiting = 1;
        }

    }




}

void CycleT::ngx_init_cycle(){
}

void CycleT::reopen_files(int user){
}

int CycleT::exec_new_binary(char**){
    return -1;
}

ngx_core_conf_t* ConfigureContext::getConf(int){
    return NULL;
}
