#include <string>
#include <map>
#include <vector>
#include <assert.h>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include "util.h"

class LogConfig{
};


struct TopicAndPartition{
  std::string topic;
  bool operator <(const TopicAndPartition & right) const{
    return topic < right.topic;
  }
};


template<typename Key, typename Value>
struct Config{
  std::map<Key,Value> _data;
  Config():_data(){};
  const Value& getOrElse(const Key&key, const Value & value){
    if(_data.count(key)){
      return value;
    }
    return _data[key];
  }
};

using CleanerConfig = std::map<std::string, std::string>;
using TopicLogConfigs = Config<std::string, LogConfig>;
using File = boost::filesystem::path;
using directory_iterator = boost::filesystem::directory_iterator;

class Log{
public:
  Log(const File & path, const LogConfig& config, int64_t offset,
      Scheduler const & scheduler, const Time & time){}
};




namespace Logg{
  TopicAndPartition parseTopicPartitionName(std::string const & Filename){
     assert(false);
     return TopicAndPartition();
  }
};

class LogManager{
  LogManager(std::vector<File> & logDirs,
	     const TopicLogConfigs& topicConfigs_,
	     const LogConfig& defaultConfig_,
	     const CleanerConfig& cleanConfig,
	     int64_t flushCheckMs,
	     int64_t flushCheckPointMs,
	     int64_t retentionCheckMs,
	     Scheduler scheduler_,
	     Time startTime_):
    defaultConfig(defaultConfig_),
    scheduler(scheduler_),
    topicConfigs(topicConfigs_), startTime(startTime_){
  }
  LogConfig defaultConfig ;
  Scheduler scheduler;
  TopicLogConfigs topicConfigs;
  std::map<TopicAndPartition, Log*> logs;
  Time startTime;

  void loadLogs(std::vector<File> dirs){
    for(auto dir : dirs){
      Config<TopicAndPartition, int64_t> recoveryPoints; //= recoveryPointCheckpoints(dir).read;
      directory_iterator end_itr;
      for(directory_iterator  dir;
	  dir != end_itr ; ++dir){
	
	if(is_directory(*dir)){
	  auto topicPartition =
	    Logg::parseTopicPartitionName(dir->path().filename().string());
	  auto config = topicConfigs.getOrElse(topicPartition.topic,
					       defaultConfig);
	  logs[topicPartition]= new Log(dir->path(), config,
				    recoveryPoints.getOrElse(topicPartition,0L),
				    scheduler,startTime);
	}
      }
      //cleanShutDownFile.del();
    }
  }

  //  Log createLog(TopicAndPartition topicAndPartition, LogConfig config){
    
    //return Log();
  //}
};

