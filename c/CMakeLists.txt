#cd ~/libs/nginx-1.15.6/ &&  ./configure --add-module=$HOME/nginxCpp/c --p\
#refix=$HOME &&  make -j  && make install

cmake_minimum_required(VERSION 3.13)


add_custom_target(nginxModule)

if(TARGET nginxModule)
  add_custom_command(TARGET nginxModule
	#COMMAND "ls > nginxModule"
  COMMAND ./configure --add-module=/home/luoxing/nginxCpp/c --prefix=/home/luoxing
  COMMAND make install
  COMMAND touch nginxModule
  WORKING_DIRECTORY ~/libs/nginx-1.15.6
  COMMENT comment)
endif()
