cmake_minimum_required(VERSION 3.8)
add_definitions (-Wno-format-security)
include(FindProtobuf)
find_package(Protobuf REQUIRED)
include_directories(${PROTOBUF_INCLUDE_DIR})



set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "./cmake")

include_directories(libs)
#include_directories(/home/luoxing/include)
#include_directories(/home/luoxing/include/c++/v1)

link_directories(/home/luoxing/lib)
link_directories(/home/luoxing/lib64)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -ggdb -fPIC -Wall")




find_path( MYSQL_INCLUDE_DIR
  NAMES "mysql.h"
  PATHS "/usr/include/mysql"
  "/usr/local/include/mysql"
  "/usr/mysql/include/mysql" )

find_library( MYSQL_LIBRARY
  NAMES "mysqlclient" "mysqlclient_r"
  PATHS "/lib/mysql"
  "/lib64/mysql"
  "/usr/lib/mysql"
  "/usr/lib64/mysql"
  "/usr/local/lib/mysql"
  "/usr/local/lib64/mysql"
  "/usr/mysql/lib/mysql"
  "/usr/mysql/lib64/mysql" )

include_directories(${MYSQL_INCLUDE_DIR})

add_subdirectory(libs/leveldb)
include_directories(libs/leveldb/include)

add_subdirectory(libs/cppkafka)
include_directories(libs/cppkafka/include)
add_subdirectory(test)

add_subdirectory(libs/ThreadPool)
include_directories(libs/ThreadPool)

add_executable(kmp
  kmp.cc
  #loger.cpp
  #Broker.cpp
  log.cpp
  
  )

add_executable(threads
  thread.cpp)
target_link_libraries(
  threads
  cppkafka rdkafka
  boost_thread
  )

# add_executable(nginxCpp
#   App.cpp
#   Cycle.cpp
#   main.cpp)



###############

# add_executable(CostModeHandTest CostModeHandTest.cpp)
# add_test(CostModeHandTest CostModeHandTest)
# target_link_libraries(CostModeHandTest gtest gtest_main )
#
