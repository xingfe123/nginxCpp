#include<map>
#include<string>
#include "util.h"
#include "LogManager.h"

using KafkaConfig = std::map<std::string, std::string>;
struct Logging{
  
};
struct ReplicaManager{};
struct KafkaController{};
struct KafkaApis{};
struct KafkaRequestHandlerPool{};
struct ZkClient{};

KafkaRequestHandlerPool
class KafkaServer :public Logging{
  KafkaServer(KafkaConfig const & config, Time time){
  }

  Scheduler kfkaScheduler;
  ZkClient zkClient;
  LogManager logManager;
  SocketServer socketServer;
  ReplicaManager replicaManager;
  KafkaController kafkaController;
  Apis apis;
  KafkaRequestHandlerPool requestHandlerPool;
  
  void startup(){
    kfkaScheduler.startup();
    zkClient = initZk();

    logManager = createLogManager(zkClient);
    logManager.startup();

    socketServer = SocketServer();

    socketServer.startup();

    replicaManager = new ReplicaManager(config, time,
					zkClient, kafkaScheduler,
					logManager, isShuttingDown);

    kafkaController = new KafkaController(config, zkClient);

    apis = new KafkaApis();

    requestHandlerPool = new KafkaRequestHandlerPool(config.brokerId,
						     socketServer.requestChan);
    
  }

  
  
};
