#include <signal.h>
#include <vector>
#include <string>
#include <map>
#include <set>

#include "Cycle.h"
#include "Util.h"

APP App ;
struct itimerval itv;

int main(){
}
struct ServiceNode{
    std::string m_strServiceDir;
    std::string serviceName;
    std::string nodeName;
    bool enable;
    std::map<std::string, std::string> properties;

    std::string serviceDir() const{
        return m_strServiceDir;
    }
    std::string servicePath() const{
        return serviceDir() + '/' + serviceName;
    }
    std::string nodePath() const{
        return servicePath() + '/' + nodeName;
    }
    std::string activePath() const{
        return nodePath() + "/active";
    }


};

struct YkServiceMng{
    static YkServiceMng* GetInstance(std::string const &host, const std::string & idc){
        return new YkServiceMng(host, idc);
    }

    std::string m_strHost;
    std::string m_strIdc;
    YkServiceMng(std::string const &host, const std::string & idc)
            :m_strHost(host), m_strIdc(idc){}
    void Register(const ServiceNode& node, bool isOK) const{
        // wildcard ip handle;
        std::vector<std::string> paths;
        paths.push_back(node.serviceDir());
        paths.push_back(node.servicePath());
        paths.push_back(node.nodePath());
        paths.push_back(node.activePath());
        for(auto keyValue : node.properties){
            if(keyValue.first == "active"){
                continue;
            }
            std::string propertyPath = node.nodePath() + '/'+ keyValue.first;
            paths.push_back(propertyPath);
        }

        std::set<std::string> existsPath;
        for(auto path :paths){
            int ret = ZooExists(path.c_str(), false);
            if(ret != ZNONODE){
                if(path.size() > node.nodePath().size()){
                    if(App.rewrite_properties || path == node.activePath()){
                        // re-create property node
                        ZooDelete(path.c_str());
                    }else{
                        existsPath.insert(path);
                    }
                }else {
                    existsPath.insert(path);
                }
            }
        }

        int opCount = paths.size() - existsPath.size();
        if(opCount <=0){
            throw "Failed";
        }

        zoo_op_t* ops = (zoo_op_t*)calloc(opCount, sizeof(zoo_op_t));
        for(auto path :paths){
            std::string propertyName;
            if(path.size() > node.nodePath().size()){
                propertyName = basename(path);
            }
            if(existsPath.contains(path)){
                continue;
            }

            if(propertyName == "active"){
                value = strOne;
                flags = ZOO_EPHEMERAL;
            }else{
                auto p = node.properties.find(propertyName);
                value = p != node.properties.end()?strOne:p->second;
            }


            if(!existsPath.contains(path)){
                zoo_create_op_init(&ops[op_index], (*path_it).c_str(), value.c_str(), static_cast<int>(value.length()), acl, flags, NULL, 0);
            } else {
                zoo_set_op_init(&ops[op_index], (*path_it).c_str(), value.c_str(), static_cast<int>(value.length()), -1, NULL);
            }
            op_index ++;
        }

        zoo_op_result_t* results = (zoo_op_result_t*)calloc(op_count, sizeof(zoo_op_result_t));
        {
            commonutil::RWRecMutex::WLock autolock(m_zhandle_lock);
            CHECK_ZHANDLE(m_zhandle);
            YK_LOG("call zoo_multi");
            ret = zoo_multi(m_zhandle, op_count, ops, results);
            // for(int i = 0; i < op_count; i ++) {
            //     YK_LOG("result %d: err: %d:%s", i, results[i].err, zerror(results[i].err));
            // }
            if(ret < 0) {
                YK_LOG("zoo_multi failed, ret: %s", zerror(ret));
                return ret;
            }
        }
        {
            commonutil::RWRecMutex::WLock autolock(m_lock);
            m_register_nodes[node_path] = node;
        }
        YK_LOG("register node success: %s", node_path.c_str());
        // ignore results.
        free(ops);
        // result->value, result->stat are all NULL here.
        free(results);
        return 0;
    }


    void DeRegister(){

    }

};
YkServiceMng * zookeeper;

void initZooKeeper(){

    zookeeper = YkServiceMng::GetInstance("zookeeper_hosts", "strMachineRoom");

    //register

    int port =100;
    std::string serviceName="test";
    std::string serviceIp="127.0.0.1";

    ServiceNode node;
    node.serviceDir = "/posbill";
    node.serviceName = serviceName;
    node.nodeName = (boost::format("%s:%d") % serviceIp % port).str();
    node.enable = true;
    zookeeper->Register(node, false);

}

void clearZooKeeper(){
    zookeeper->DeRegister();
}
