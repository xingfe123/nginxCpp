#pragma once

struct Logger{
    int fd;
    int file;
    int* next;
    int* writer;
    Logger* get_file_log(){
        return this;
    }
};
