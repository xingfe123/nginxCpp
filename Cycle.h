#pragma once
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <string>
#include <stdlib.h>
#include "Logger.h"
#include "App.h"



struct Pool{
    char* pnalloc(int size){
      return (char*)malloc(size);
    }
    void destroy(){
    };

    ~Pool(){
        destroy();
    }
};

const int ngx_core_module = -1;

struct pidData{
    char* data;
};

struct ngx_core_conf_t{
    int worker_processes;
    int user;
    pidData oldpid;
    pidData pid;
};

struct ConfigureContext{
    ngx_core_conf_t* getConf(int ngx_core_module);
    int user;
};

const char master_process[]="master_process";

struct Fd{
    int fd;
};


struct Listening{
    Fd *elts;
    int nelts;
};

struct CycleT;

struct Module{
    bool operator()() const{
        return false;
    }
    int (*exit_master)(CycleT *);

};


struct ngx_channel_t{
    int command;
    int pid;
    int slot;
    int fd;
};


struct CycleT{
    int  masterProcess();
    void  start_worker_processes(int, int);
    void signal_worker_processes(int);
    void master_process_exit();
    void  start_cache_manager_processes(int);

    void ngx_init_cycle();
    void reopen_files(int users);
    void close_listening_sockets();
    void ngx_delete_pidfile();
    int exec_new_binary(char ** argv);
    int spawn_process(int cycle, void* , const std::string workerName, int type);
    void pass_open_channel(ngx_channel_t &ch);
    int ngx_reap_children();

    Pool pool;
    Logger log;
    ConfigureContext configureContext;
    Listening listening;
    Module** modules;
};
