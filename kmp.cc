#include <vector>
#include <string>
#include <assert.h>

auto computeNext(const std::string & word){
    std::vector<int> nextIndex(word.size());
    nextIndex[0] = -1;
    
    int t=nextIndex[0];
    int index=0;
    while(index < word.size()){
        if(t <0 || word[index] == word[t]){
            nextIndex[++index] = ++t;
        }else{
            t = nextIndex[t];
        }
    }
    return nextIndex;
}

auto strstr(std::string const & left, std::string const & right){
    assert(left.size() >= right.size());
    if(left.empty() || right.empty()){
        return 0;
    }
    
    auto next = computeNext(right);

    int leftIndex = 0;
    int rightIndex = 0;
    while(leftIndex < left.size()
          && rightIndex < right.size()){
        if(rightIndex<0 || left[leftIndex] == right[rightIndex]){
            rightIndex++;
            leftIndex++;
        }else{
            rightIndex = next[rightIndex];
        }
    }

    if(rightIndex == right.size()){
        return leftIndex-rightIndex;
    }
    return -1;
}



struct FiniteAutomatinMatcher{

    static auto isSuffix(std::string const str, std::string const & suffix){
        assert(str.size() >= suffix.size());

        return str.substr(str.size() - suffix.size()) == suffix;
    }
    // int[][] compteTransitionFunction(std::string const p, int size){
    //     int[][] detla = new int;
        
    // }
};
FiniteAutomatinMatcher matcher;




int main(){
    
}
